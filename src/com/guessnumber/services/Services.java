package com.guessnumber.services;

import java.util.Scanner;

public class Services {

    private static final String EXIT = "exit";
    private static final String IN_MIN_NUM = "Enter the minimum number of the range(from 1 to 199):";
    private static final String IN_MAX_NUM = "Enter the maximum number of the range(from ";
    private static final String IN_ATTEMPTS_COUNT = "Enter the count of attempts(from 1 to 15)";
    private static final String ORDER = "Please, enter number that you guess or <<exit>>!";
    private static final String INCORRECT_NUM = "Number out of range! Please, input correct number!";
    private static final String GOODBYE = "Program is closed. Goodbye!";

    private static Scanner scanner = new Scanner(System.in);

    int min;
    int max;
    int attempts;
    int randNumber;
    int tries = 1;
    int numInput;
    String input;

    public void inputMinMaxNum() {
        System.out.println(IN_MIN_NUM);
        min = scanner.nextInt();
        if (min < 1 || min > 200) {
            System.out.println(INCORRECT_NUM);
            inputMinMaxNum();
        }
        System.out.println(IN_MAX_NUM + (min+1) + " to 200):");
        max = scanner.nextInt();
        if (max <= min || max > 200) {
            System.out.println(INCORRECT_NUM);
            inputMinMaxNum();
        }
    }

    public void inputCountAttempts() {
        System.out.println(IN_ATTEMPTS_COUNT);
        attempts = scanner.nextInt();
        if (attempts < 1 || attempts > 15) {
            System.out.println(INCORRECT_NUM);
            inputCountAttempts();
        }
    }

    public void generateNumber() {
        randNumber = (int) ((Math.random()*(max-min)+min));
        System.out.println("Hello! I guess number from " + min + " to " + max + " of your range, try to guess in "
                + attempts + " tries!");
        System.out.println("My rand: " + randNumber);
    }

    public void launchInterface() {
        System.out.println();
        input = scanner.next();

        if (input.equals(EXIT)) {
            System.out.println(GOODBYE);
            System.exit(0);
        } else if (isNumeric(input)) {
            getAttempts();
        } else {
            System.out.println("WRONG INPUT! " + ORDER);
        }
    }

    private void getAttempts() {
        while (tries < attempts) {
            numInput = Integer.parseInt(input);
            System.out.println(isNumeric(input));
            System.out.println(numInput);
            if (numInput == randNumber) {
                System.out.println("Congratulations! You guessed number in " + tries + " tries!");
                System.exit(0);
            } else if (numInput > randNumber && numInput >= min && numInput <= max) {
                System.out.println("Guessed wrong, but colder! You have " + (attempts - tries) + " tries!");
                tries++;
                launchInterface();
            } else if (numInput < randNumber && numInput >= min && numInput <= max) {
                System.out.println("Guessed wrong, but warmer! You have " + (attempts - tries) + " tries!");
                tries++;
                launchInterface();
            } else {
                System.out.println("Guessed wrong!!! You out of range!!! You have " + (attempts - tries) + " tries!");
                tries++;
                launchInterface();
            }
            if (tries >= attempts && numInput != randNumber) {
                System.out.println("You have run out of attempts! You didn't guess the number!");
                System.exit(0);
            }
        }
    }

    private boolean isNumeric (String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}




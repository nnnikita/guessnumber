package com.guessnumber;

import com.guessnumber.services.Services;

public class Main {

    public static void main(String[] args) {

        Services services = new Services();

        services.inputMinMaxNum();
        services.inputCountAttempts();
        services.generateNumber();
        while (true) {
            services.launchInterface();
        }
    }
}